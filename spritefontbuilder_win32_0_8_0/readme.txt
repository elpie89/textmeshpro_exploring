******************************************************* 
* Application: Sprite Font Builder
* Original Source: http://www.johnwordsworth.com/
* Author: John Wordsworth <john@johnwordsworth.com>
*******************************************************

******************************************************* 
Introduction
*******************************************************

Sprite Font Builder is a cross-platform application for generating fancy bitmap
fonts for use primarily in games built with engines and frameworks that support
Bitmap Fonts such as Cocos2d.

I built this application to properly get to know Qt before embarking on a more
complicated project but I also wanted to make sure that it stood out from the
crowd of other Bitmap Font generators. The unique-selling point for this Sprite
Font Builder is the ability to add any number of FX layers to your sprite font
so that you can create fancy fonts that look similar to images built using
‘Photoshop FX Layers’ – except the effect is applied to each glyph individually,
instead of the whole image layer.

Sprite Font Builder currently supports Fill (Solid, Gradient and Pattern),
Shadow (Inner and Outer) and Stroke (Inner, Centred, Outer) effects and along
with a variety of blend modes, opacity sliders and settings you can generate
fairly complicated bitmap fonts. For example, using light and dark inner shadows
you can emulate 3D / embossed characters and with over-layed gradient inset
fills that fade to transparent to make a cartoony shine effect in the inner
section of each character.

First Release Disclaimer: This is the first release of this software. It might
have bugs which cause it to not work properly on your system. As it only really
works with it’s own files, the risk of using this software is low (if it saves a
corrupt file, hey ho – it’s not the end of the world). Hopefully everything is
good, but you never know!

******************************************************* 
Download
*******************************************************

Sprite Font Builder is a completely free application as it was
built in my spare time as a way to learn Qt. I intend to open source it very
soon, but before uploading the code to Github I would like to spend some time
tidying up the source code (yeh, bet you’ve never heard that before!).

You can download the latest binary versions of the application at;
http://www.johnwordsworth.com/projects/sprite-font-builder/

While distributed for free, if you do many a game that uses fonts built with 
Sprite Font Builder, I wouldn’t be adverse to a free download code for your 
app as a thank you!
 
******************************************************* 
Features
*******************************************************

Apart from the obvious, the primary features of this Sprite Font Builder are as
follows:

Multiple Effect Layers

You can create sprite fonts using multiple layered effects. Fill layers support
flat colour, gradients and pattern fills and all effect layers can be blended
using one of a dozen blend modes so that you can create overlayed patterns
multiplied onto a base layer. Fills can also be inset to promote a feeling of
depth. Shadow effects support drop shadow and inner- shadow modes with varying
distance, size and blur. Lastly, a standard stroke effect can be used to finish
off the effect with a defined edge.

Load / Save SFB Files

Your Sprite Fonts can be saved in a human readable XML format so that you can
easily re-open your font later on and tweak settings / re-export them. Pattern
source images are saved in the file to make it fully portable. As export
settings are also saved as part of the file, making a tweak to your font is as
easy as opening the file, changing a value and clicking ‘Export’.

Retina Export

By checking a single box you can export both regular sized and retina sized (x2)
versions of your fonts. All of your effects will be scaled so that both versions
of the fonts look the same.

******************************************************* 
Current Version Notes
*******************************************************

There are a few outstanding issues with the current release. They are listed below.

* Not all fonts have the correct kerning information. I believe that Qt isn’t
loading it, but I’m looking into this.

* There’s no undo system. There may never be, but it’s on the list (albeit it
with a low priority).

* There’s no inbuilt preview system for your created fonts. This is on the list,
with a high priority.

* Font generation is controlled by the GUI thread (even though it spawns
multiple threads to speed up the work). Complex fonts might get slow and painful
to edit – in this case, work on a cut down set of characters and add the full
set when you’re happy.

* Currently, only ‘Ascii Bitmap Fonts’ are generated. More export formats are in
the works.

******************************************************* 
Bug Reports / Feature Requests
*******************************************************

If you find any bugs or would like to see a new feature added to this software,
you can chat wth my on Twitter (http://www.twitter.com/JohnWordsworth), contact
me through my website (http://www.johnwordsworth.com/contact-me/) or leave a 
comment on my blog (http://www.johnwordsworth.com).

******************************************************* 
Roadmap
*******************************************************

* Additional export file formats.

* Ability to preview strings built using your font in the app.

* Move font generation to a background thread so tweaking options on a complex
font is more friendly. Undo system (low priority – will add if the app becomes
popular!).

* Add some preset font styles. GUI improvements.

******************************************************* 
License
*******************************************************
 
(GPL v3) Sprite Font Builder is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.

******************************************************* 
Dependencies
*******************************************************

Plist Pad was built using the Qt Widget Library and uses images from the Open
Icon Library.

******************************************************* 
Disclaimer
*******************************************************

This software is provided “as is” and any expressed or implied warranties,
including, but not limited to, the implied warranties of merchantability and
fitness for a particular purpose are disclaimed. In no event shall the regents
or contributors be liable for any direct, indirect, incidental, special,
exemplary or consequential damages (including, but not limited to procurement of
substitute goods or services; loss of use, data or profits; or business
interruption) however caused and on any theory of liability, whether in
contract, strict liability or tort (including negligence or otherwise) arising
in any way out of the use of this software, even if advised of the possibiltiy
of such damage.
